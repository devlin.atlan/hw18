#include <iostream>
#include <string>  
#include "main.h"
using namespace std;
/*
+ 	��������� �������� ������ ��� �������� ����� � ������������ ������ Stack. 
+	����� ����� �� ������������ � ������������ �� ��������� ������������ �����-�� ������������� �������� �������, �������� 10.
+	� ���������� � �����, ����� �������� ����������� � ����������. � �������� ��������� ����� ������ �����.
+	��� ���� ���� ������ arr ���������� �������� �� ���������(T* arr). 
+	����� �������� ��� ���� ���� ������, ������� ����� ������� ������ �������� ��������, ��� ��� CurrentSize ������ ������ � �� �� ������ ����������.
+	����� �������, ������ CurrentSize ����� ���������� ����� ���� ������ ��� ������� Push � Pop.

+	� �� �������������� ��������� ���������� ������ Push �� ������ ���� ������ ���������� ������ �������� ������������� ��� ���������� ���������� ��������. 
+	����� ����������� ����� �������� ������ ��� ������ new � ������� ����� ���������� ��� ������ delete.

+	�������� ���������� ������, ����� ������ ��������� ��� ��� ������. ���������� ��������� ������������� ��� ������ �� main.
*/
template <typename T> class Stack
{
public:
	//�����������, ������ ������ �������� � ����������� ������ � ��� ���� ��������� ������
	Stack(int n = 10)
	{
	//����� �������� �������� ������� �������
		ArrSize = n;
	//������� ������ ��� ������ ��������� �������
		arr = new T[ArrSize];
	}
	//���������� ��� ������� ������
	~Stack()
	{
		delete[] arr;
	}

	//�������� ������� � ����
	void Push(T  a)
	{
		//���� ������ ��������, ������ �� ���� ������, ������� ��� ������ �� ����� ������� ���������� ���������,
		// � ������ ������ �� ���������� ������ �� ������ ������
		if (Head+1 == ArrSize)
		{
			ArrSize *= 2;
			T* arrToDelete = arr; 
			arr = new T[ArrSize]{*arr};
			delete[] arrToDelete; 
		} 
			Head++;
			arr[Head] = a; 
	}
	//������� ������� �� �����
	void Pop()
	{
		if (Head == -1) { return; } 
		Head--;
	} 
	//������� �������� �� �����
	T Peek()
	{
		//���� ���� ������, ������������ ������ �������� ���������������� ���� (������� ����� � stackoverflow)
		if (Head == -1) {
			return  T();// NULL;
		}
		return arr[Head];
	}
	//��������, �� ����� �������� ������ �����
	int GetHead()
	{
		return Head;
	}
	//�������� ���� � ������, � ����� �������� ��� ���� ����� ������� ������
	void Clear()
	{
		Head = -1;
		delete[] arr;
		arr = new T[ArrSize]; 
	}
private:
	//��������� �� ������ �����, ���������� �� �� ��� �� ���������
	int Head = -1;
	//������ ������� ��� �������� �����
	int ArrSize = 10;
// ��������� �� ������, �������� �������� �����. ���� �������� ����������, ����� 
// ��� ������� ���������� ��� ���������������� ������ ������ ������� ����\�������
	T* arr;  
};

//������ ������: ��� ������ ����� Int, Float, Double, String
void TestStackInt(Stack<int>& S)
{
	S.Push(1);
	S.Push(4);
	S.Push(5);
	S.Push(6);
	std::cout << (S.Peek()) << "\n";
	S.Pop();
	std::cout << S.Peek() << "\n";
	std::cout << std::to_string(S.GetHead()) << "\n";
	S.Clear();
	std::cout << std::to_string(S.Peek()) << "\n";
	std::cout << std::to_string(S.GetHead()) << "\n";
	S.Push(25);
	std::cout << std::to_string(S.Peek()) << "\n\n\n";
}

void TestStackFloat(Stack<float>& S)
{
	S.Push(1.0002);
	S.Push(4.234234);
	S.Push(5.23423);
	S.Push(6.243324);
	std::cout << std::fixed << (S.Peek()) << "\n";
	S.Pop();
	std::cout << S.Peek () << "\n";
	std::cout << std::to_string(S.GetHead()) << "\n";
	S.Clear();
	std::cout << std::fixed << (S.Peek()) << "\n";
	std::cout <<   (S.GetHead()) << "\n";
	S.Push(4.234234f);
	std::cout << std::fixed << (S.Peek()) << "\n\n\n";
}

void TestStackDouble(Stack<double>& S)
{
	S.Push(1321.321312);
	S.Push(43321.123213);
	S.Push(52323.23423423);
	S.Push(6.7737218248437437654);
	cout.precision(8);
	std::cout << std::fixed << (S.Peek()) << "\n";
	S.Pop();
	std::cout << std::fixed << S.Peek() << "\n";
	std::cout <<  (S.GetHead()) << "\n";
	S.Clear();
	std::cout << std::fixed << (S.Peek()) << "\n";
	std::cout << std::fixed << (S.GetHead()) << "\n";
	S.Push(43321.123213);
	std::cout << std::fixed << (S.Peek()) << "\n\n\n";
}
void TestStackString(Stack<string>& S)
{
	S.Push((string)"ammm");
	S.Push((string)"dmmmm");
	S.Push((string)"fmmmmmm");
	S.Push((string)"Hmmmmmmmmm");
	std::cout << (S.Peek()) << "\n";
	S.Pop();
	std::cout << S.Peek() << "\n";
	std::cout << (S.GetHead()) << "\n";
	S.Clear();
	std::cout << static_cast<string>(S.Peek()) << "\n";
	std::cout << (S.GetHead()) << "\n";
	S.Push((string)"dmmmm");
	std::cout << (S.Peek()) << "\n\n\n";
}
int main()
{
	//�������� ����������� ������ ����� � �������� ����� ������
	//����� ����� ���� ����������������� ����� (���� ������ ���������� - ��� �����������)
	Stack<int>  MyStackInt;// = new Stack<int>();
	TestStackInt(MyStackInt);

	Stack<float> MyStackFloat;// = new Stack<float>();
	TestStackFloat(MyStackFloat);

	Stack<double> MyStackDouble;// = new Stack<double>();
	TestStackDouble(MyStackDouble);

  	Stack<string>* MyStackString = new Stack<string>(20);
 	TestStackString(*MyStackString);
	delete MyStackString;

	system("pause");
	return 0;
}